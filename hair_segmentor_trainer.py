# Autoencoder to segment hair and face from background
# Image sizes have been reduced to allow for quicker training and testing
# Images are normalised to -1 to 1 range, this should allow faster convergence with the tanh activation function
# Output image red channel is the face segmentation mask
# Output image green channel is the hair segmentation mask

# Model converged at around 0.068 loss, and at around 10000 Epocs 
# Sometimes eyebrows are not segmented properly - although the model is not technically wrong as some of the label data does not have eyebrows segmented correctly
# Could improve by using larger images, more training data, performing random crops/rotations,scaling etc to augment data set

# Imports
import cv2
import tensorflow as tf
import tensorflow.contrib.layers as lays
import numpy as np
import glob
import os

# Useful global variables
imgSize = 64
numChannels = 3
numEpochs = 5000
learningRate = 0.001
summarySize = 250
filterSize = 3

# Function to retun image dataset as numpy array
def loadDataset(globCriteria):
	imagePaths = glob.glob(globCriteria)
	images = np.empty((0,imgSize,imgSize,numChannels))
	for imgPath in imagePaths:
		img = cv2.imread(imgPath)
		img = cv2.resize(img, (imgSize, imgSize))
		img = img.reshape((1,imgSize,imgSize,numChannels))
		img = np.interp(img,[0, 255],[-1, 1])
		images = np.vstack([images, img])
	return images	

# Load up and batch together all input image files
inputImages = loadDataset('hair_test/*.jpg')
print "Number of training data: ", len(inputImages)
print "Input data tensor shape: ", inputImages.shape

# Load up and batch together all label image files
outputImages = loadDataset('hair_test/*.png')
print "Number of training labels: ", len(outputImages)
print "Output data tensor shape: ", outputImages.shape

# Here is the tensorflow placeholder for data input
inputData = tf.placeholder(tf.float64, shape=[None, imgSize, imgSize, numChannels])

# Define the actual autoencoder network here
# Encoder part
net = lays.conv2d(inputImages, 64, [filterSize, filterSize], stride=2, padding='SAME')
net = lays.conv2d(net, 32, [filterSize, filterSize], stride=2, padding='SAME')
net = lays.conv2d(net, 16, [filterSize, filterSize], stride=2, padding='SAME')
net = lays.conv2d(net, 8, [filterSize, filterSize], stride=4, padding='SAME')

# Decoder part
net = lays.conv2d_transpose(net, 16, [filterSize, filterSize], stride=4, padding='SAME')
net = lays.conv2d_transpose(net, 32, [filterSize, filterSize], stride=2, padding='SAME')
net = lays.conv2d_transpose(net, 64, [filterSize, filterSize], stride=2, padding='SAME')
net = lays.conv2d_transpose(net, 3, [filterSize, filterSize], stride=2, padding='SAME', activation_fn=tf.nn.tanh)

# Loss function - mean squared error
loss = tf.reduce_mean(tf.square(net - outputImages))

# Optimiser - Adam
trainingOp = tf.train.AdamOptimizer(learningRate).minimize(loss)

# Create the saver object
saver = tf.train.Saver()

# Training loop here
with tf.Session() as sess:
	sess.run(tf.global_variables_initializer())
	for i in range(numEpochs):
		_, c = sess.run([trainingOp, loss], feed_dict={inputData: inputImages})
		if i % summarySize == 0:
			print "Epoch: ", i
			print "Loss: ", c

        print "Training Complete"
	# Save the model to disk
	save_path = saver.save(sess, os.getcwd() + '/hair_segmentor_model.ckpt')
	print("Model saved in path: %s" % save_path)
