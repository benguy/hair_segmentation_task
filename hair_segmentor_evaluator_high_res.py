# Autoencoder to segment hair and face from background
# Image sizes have been reduced to allow for quicker training and testing
# Images are normalised to -1 to 1 range, this should allow faster convergence with the tanh activation function
# Output image red channel is the face segmentation mask
# Output image green channel is the hair segmentation mask

# Model converged at around 0.02 loss for high res model, and at around 2500 Epocs 
# Sometimes eyebrows are not segmented properly - although the model is not technically wrong as some of the label data does not have eyebrows segmented correctly
# Could improve by using larger images, more training data, performing random crops/rotations,scaling etc to augment data set

# Imports
import cv2
import tensorflow as tf
import tensorflow.contrib.layers as lays
import numpy as np
import glob
import os

# Useful global variables
imgSize = 128
numChannels = 3
learningRate = 0.0001
filterSize = 3

# Function to retun image dataset as numpy array
def loadDataset(globCriteria):
	imagePaths = glob.glob(globCriteria)
	images = np.empty((0,imgSize,imgSize,numChannels))
	for imgPath in imagePaths:
		img = cv2.imread(imgPath)
		img = cv2.resize(img, (imgSize, imgSize))
		img = img.reshape((1,imgSize,imgSize,numChannels))
		img = np.interp(img,[0, 255],[-1, 1])
		images = np.vstack([images, img])
	return images	

# Load up and batch together all input image files
inputImages = loadDataset('hair_test/*.jpg')
print "Number of training data: ", len(inputImages)
print "Input data tensor shape: ", inputImages.shape

# Load up and batch together all label image files
outputImages = loadDataset('hair_test/*.png')
print "Number of training labels: ", len(outputImages)
print "Output data tensor shape: ", outputImages.shape

# Here is the tensorflow placeholder for data input
inputData = tf.placeholder(tf.float64, shape=[None, imgSize, imgSize, numChannels])

# Define the actual autoencoder network here
# Encoder part
net = lays.conv2d(inputImages, 128, [filterSize, filterSize], stride=2, padding='SAME')
net = lays.conv2d(net, 64, [filterSize, filterSize], stride=2, padding='SAME')
net = lays.conv2d(net, 32, [filterSize, filterSize], stride=2, padding='SAME')
net = lays.conv2d(net, 16, [filterSize, filterSize], stride=2, padding='SAME')
net = lays.conv2d(net, 8, [filterSize, filterSize], stride=4, padding='SAME')

# Decoder part
net = lays.conv2d_transpose(net, 16, [filterSize, filterSize], stride=4, padding='SAME')
net = lays.conv2d_transpose(net, 32, [filterSize, filterSize], stride=2, padding='SAME')
net = lays.conv2d_transpose(net, 64, [filterSize, filterSize], stride=2, padding='SAME')
net = lays.conv2d_transpose(net, 128, [filterSize, filterSize], stride=2, padding='SAME')
net = lays.conv2d_transpose(net, 3, [filterSize, filterSize], stride=2, padding='SAME', activation_fn=tf.nn.tanh)

# Loss function - mean squared error
loss = tf.reduce_mean(tf.square(net - outputImages))

# Optimiser - Adam
trainingOp = tf.train.AdamOptimizer(learningRate).minimize(loss)

# Create the saver object
config = tf.ConfigProto(
        device_count = {'GPU': 0}
    )
saver = tf.train.Saver()

# Training loop here
with tf.Session(config=config) as sess:
	saver.restore(sess, os.getcwd() + '/hair_segmentor_model_high_res.ckpt')

	# Show some images to prove that it worked
    # This isnt the nicest way to go about things but its just here for visualisation
	labelImages = np.hstack([outputImages[0,:,:,:], outputImages[1,:,:,:], outputImages[2,:,:,:]])
	labelImages = np.interp(labelImages,[-1,1],[0, 255]).astype(np.uint8)

	outputImages = net.eval(feed_dict={inputData: inputImages})
	outputImages = np.hstack([outputImages[0,:,:,:], outputImages[1,:,:,:], outputImages[2,:,:,:]])
	outputImages = np.interp(outputImages,[-1,1],[0, 255]).astype(np.uint8)

	inputImages = np.hstack([inputImages[0,:,:,:], inputImages[1,:,:,:], inputImages[2,:,:,:]])
	inputImages = np.interp(inputImages,[-1,1],[0, 255]).astype(np.uint8)

	cv2.imshow('Resulting Images', np.vstack([inputImages, labelImages, outputImages]))
	cv2.waitKey()

    # Generate a per pixel confusion matrix
	trueBackground, trueHair, trueFace = 0,0,0
	predBackground, predHair, predFace = 0,0,0
    # Opencv uses BGR, so channel 1 is hair (green)
	labelHair = labelImages[:,:,1]
	outputHair = outputImages[:,:,1]
    # Channel 2 is face (red)
	labelFace = labelImages[:,:,2]
	outputFace = outputImages[:,:,2]

    # Didn't quite get to finish the coding for creating a confusion matrix, but the plan was to binarise the above segmentation masks, iterate through them pixel by pixel and to determine true positives, false positives, true negatives and false negatives for each of the three classes. That way I could have easily plotted a confusion matrix for the 3 segmentation maps, with respect to every pixel.
